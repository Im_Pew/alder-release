package hr.dturic.personal.tdkappdemoadler

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import hr.dturic.personal.tdkappdemoadler.cards.CardsFragment
import hr.dturic.personal.tdkappdemoadler.common.Navigation
import hr.sevenofnine.agency.alder.AlderFiles
import hr.sevenofnine.agency.alder.log.Alder
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"

        private const val DELAY = 1000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Navigation.replace(CardsFragment())
        Alder.init(this)

        MainScope().launch {
            Alder.log(TAG, "First log")
//            Alder.loginUser("13254")
//            Alder.logoutUser()
            return@launch
            // Initial app run
            Alder.log(TAG, "First log")
            Alder.log(TAG, "First transactions log", AlderFiles.TRANSACTIONS)
            delay(DELAY)

            // Sign in user
            Alder.loginUser("51423")
            Alder.log(TAG, "First log with userId")
            Alder.log(TAG, "Logged user transactions log", AlderFiles.TRANSACTIONS)
            delay(DELAY)

            // Logout user
            Alder.logoutUser()
            Alder.log(TAG, "Logout user")
        }
    }
}