package hr.dturic.personal.tdkappdemoadler.common.base

interface StateListener {

    fun onSuccess()
    fun onLoading(show: Boolean)
    fun onError(error: String)
}