package hr.dturic.personal.tdkappdemoadler.api

import hr.dturic.personal.tdkappdemoadler.model.Card
import hr.dturic.personal.tdkappdemoadler.model.CardDetails
import retrofit2.http.GET
import retrofit2.http.Path

interface CardsApi {

    @GET("cards_json.json")
    suspend fun getCards(): List<Card>

    @GET("cards/{id}.json")
    suspend fun getCard(@Path("id") id: String): CardDetails
}