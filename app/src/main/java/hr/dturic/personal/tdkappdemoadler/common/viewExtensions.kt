package hr.dturic.personal.tdkappdemoadler.common

import android.view.View

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.invisibleIf(condition: Boolean) {
    if (condition) invisible()
    else visible()
}

fun View.goneIf(condition: Boolean) {
    if (condition) gone()
    else visible()
}