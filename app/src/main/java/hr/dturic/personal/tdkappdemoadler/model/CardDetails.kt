package hr.dturic.personal.tdkappdemoadler.model

import com.squareup.moshi.Json

data class CardDetails(
    @field:Json(name = "card_id") val cardId: String,
    @field:Json(name = "card_code") val cardCode: String,
    @field:Json(name = "card_expiration_date") val expirationDate: String,
    @field:Json(name = "card_logo") val logo: String,
    @field:Json(name = "bank_name") val bankName: String,
    @field:Json(name = "card_owner") val cardOwner: String,
    @field:Json(name = "can_send") val canSend: Boolean,
    @field:Json(name = "can_receive") val canReceive: Boolean,
    @field:Json(name = "card_name") val cardName: String
)
