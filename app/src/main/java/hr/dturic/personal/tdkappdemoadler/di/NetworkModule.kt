package hr.dturic.personal.tdkappdemoadler.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.dturic.personal.tdkappdemoadler.BuildConfig
import hr.dturic.personal.tdkappdemoadler.api.CardsApi
import hr.dturic.personal.tdkappdemoadler.log.LogInterceptor
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {
    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun provideOkHttpClient(authInterceptor: Interceptor, logInterceptor: LogInterceptor) =
        OkHttpClient
            .Builder()
            .addInterceptor(authInterceptor)
            .addInterceptor(logInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideAuthInterceptor() =
        object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val request = chain.request()
                val build = request.newBuilder()
                    .addHeader("Accept", "application/json")
                    .build()

                return chain.proceed(build)
            }
        }

    @Provides
    @Singleton
    fun provideLoggingInterceptor() = LogInterceptor()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        BASE_URL: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun provideScope() = CoroutineScope(CoroutineName("tdk_app_demo") + Dispatchers.IO)

    @Provides
    fun provideCardsApi(retrofit: Retrofit) = retrofit.create(CardsApi::class.java)
}