package hr.dturic.personal.tdkappdemoadler.views

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import hr.dturic.personal.tdkappdemoadler.R
import kotlinx.android.synthetic.main.view_send_receive.view.*

class SendReceiveView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var sendEnabled: Boolean = false
        set(value) {
            field = value
            if (value)
                send.setImageResource(R.drawable.ic_send_active)
            else
                send.setImageResource(R.drawable.ic_send)
        }

    var receiveEnabled: Boolean = false
        set(value) {
            field = value
            if (value)
                receive.setImageResource(R.drawable.ic_receive_active)
            else
                receive.setImageResource(R.drawable.ic_receive)
        }

    init {
        View.inflate(context, R.layout.view_send_receive, this)

        var ta: TypedArray? = null
        try {
            ta = getContext().obtainStyledAttributes(attrs, R.styleable.SendReceiveView)
            sendEnabled = ta.getBoolean(R.styleable.SendReceiveView_send, false)
            receiveEnabled = ta.getBoolean(R.styleable.SendReceiveView_receive, false)
        } finally {
            ta?.recycle()
        }
    }

    fun set(canSend: Boolean, canReceive: Boolean) {
        sendEnabled = canSend
        receiveEnabled = canReceive
    }

    fun getStatusAsString(): String {
        val states = mutableListOf<String>()
        if (sendEnabled) states.add("slati")
        if (receiveEnabled) states.add("primati")

        if (states.isEmpty()) return "ništa"

        return states.joinToString(" i ") + " novce"
    }
}