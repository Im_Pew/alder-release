package hr.sevenofnine.agency.alder.log

import android.content.Context
import hr.sevenofnine.agency.alder.AlderConfig.AUTH_URL
import hr.sevenofnine.agency.alder.AlderConfig.SECRET_KEY
import hr.sevenofnine.agency.alder.AlderFiles
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

internal object AlderUrlUtil {

    @Throws(Exception::class)
    private suspend fun retrieveUrl(context: Context, alderCategory: AlderFiles?): String {
        val deviceId =
            AlderPreferences.getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = AlderPreferences.getUserId(context)
        val category = alderCategory?.getLogFileName()

        return withContext(MainScope().coroutineContext + Dispatchers.Default) {
            with(URL(AUTH_URL).openConnection() as HttpsURLConnection) {
                requestMethod = "POST"
                addRequestProperty("Authorization", SECRET_KEY)
                addRequestProperty("Content-Type", "application/json")

                val requestData = JSONObject().apply {
                    put("deviceId", deviceId)
                    userId?.let { put("userId", it) }
                    category?.let { put("category", it) }
                }

                with(outputStream) {
                    write(requestData.toString().toByteArray())
                    flush()
                }

                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }

                    val link = response.toString()
                    AlderPreferences.saveLink(context, link, alderCategory)
                    link
                }
            }
        }
    }

    internal suspend fun getUrl(context: Context, category: AlderFiles?): String {
        return AlderPreferences.getLink(context, category) ?: return retrieveUrl(context, category)
    }

    internal fun generateTag(
        deviceId: String,
        category: String = "*",
        userId: String = "*"
    ) = "${deviceId}_${userId}_${category}"
}