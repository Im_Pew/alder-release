package hr.sevenofnine.agency.alder.log

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import hr.sevenofnine.agency.alder.AlderFiles
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection

object Alder {
    private val scope = CoroutineScope(Dispatchers.Default + CoroutineName("Alder"))

    private lateinit var context: Context

    private val END_LINE =
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            System.lineSeparator().toByteArray()
        } else {
            (System.getProperty("line.separator") ?: "\n").toByteArray()
        }

    /**
     * Call this method so Alder can be initialized and used through app
     */
    fun init(context: Context) {
        this.context = context
        var deviceId = AlderPreferences.getDeviceId(context)

        AlderCacheManager.init(context)

        if (deviceId == null) {
            deviceId = retrieveDeviceId(context)
            AlderPreferences.saveDeviceId(context, deviceId)
        }
    }

    /**
     * Use this method when user is logged into app. When method is called all cached files
     * with log data that represents state without user are
     * sent to server and new ones are created, that contains userId
     *
     * @param userId String value of user ID
     */
    fun loginUser(userId: String) {
        if (!::context.isInitialized) throw RuntimeException("Call Alder.init(Context) first")
        clearLinks()
        AlderPreferences.saveUserId(context, userId)
    }


    /**
     * Use this method when user is logged out from app. When method is called all cached files
     * with log data that represents state with logged user are
     * sent to server and new ones are created, that don't contains userId
     */
    fun logoutUser() {
        if (!::context.isInitialized) throw RuntimeException("Call Alder.init(Context) first")
        clearLinks()
        AlderPreferences.removeUser(context)
    }

    private fun clearLinks() {
        val oldUserId = AlderPreferences.getUserId(context)
        AlderPreferences.removeLink(context, "*", oldUserId)
        AlderFiles.values().forEach {
            AlderPreferences.removeLink(context, it.getLogFileName(), oldUserId)
        }
    }

    /**
     * Call this method when you wanna send log, log is cached locally before it is sent to server
     * Log will be sent in time intervals that are configured in config file, or if log file reach
     * size that is also set in config file. If caching interval or max cache file size isn't specified
     * in config data interval is set to 60 seconds, and file size is set to 100 kB
     *
     * @param tag Source of log (example: MainActivity, LoginFragment...)
     * @param log Body of log (example: User entered wrong PIN 3 times...)
     * @param category Category into which log should be saved, if category isn't provided log is saved to general
     */
    fun log(tag: String, log: String, category: AlderFiles? = null) =
        log("${formatCurrentTimestamp()}:$tag/$log", category)

    /**
     * Send log data to cache
     */
    internal fun log(body: String, category: AlderFiles? = null) {
        AlderCacheManager.cache(body, category)
    }

    /**
     * Method returns ids for current device and user (if user is currently logged, or * if there no logged user)
     */
    fun getIds(): String {
        if (!::context.isInitialized) throw RuntimeException("Call Alder.init(Context) first")
        return "${AlderPreferences.getDeviceId(context)} - ${AlderPreferences.getUserId(context) ?: "*"}"
    }

    /**
     * Send data to server or cache it again if there is some problems
     */
    internal fun sendLog(body: String, category: AlderFiles? = null) {
        if (!::context.isInitialized) throw RuntimeException("Call Alder.init(Context) first")
        scope.launch(Dispatchers.Default) {
            try {
                val url = AlderUrlUtil.getUrl(context, category)
                sendLog(url, body)
            } catch (e: Exception) {
                AlderCacheManager.cache(body, category)
                println(e.message)
            }
        }
    }

    private fun formatCurrentTimestamp(): String {
        val simpleDateFormat = SimpleDateFormat("HH:mm:ss.SSS dd:MM:yyyy", Locale.getDefault())
        return simpleDateFormat.format(Date())
    }

    @Throws(Exception::class)
    private fun sendLog(url: String, log: String) {
        with(URL(url).openConnection() as HttpsURLConnection) {
            requestMethod = "PUT"

            with(outputStream) {
                write(log.toByteArray())
                write(END_LINE)
                flush()
            }

            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()

                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
            }
        }
    }

    @SuppressLint("HardwareIds")
    private fun retrieveDeviceId(context: Context): String = Settings.Secure.getString(
        context.contentResolver, Settings.Secure.ANDROID_ID
    )
}