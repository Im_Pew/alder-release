package hr.sevenofnine.agency.alder.prebuild;

import com.squareup.kotlinpoet.ClassName;
import com.squareup.kotlinpoet.CodeBlock;
import com.squareup.kotlinpoet.FileSpec;
import com.squareup.kotlinpoet.FunSpec;
import com.squareup.kotlinpoet.ParameterizedTypeName;
import com.squareup.kotlinpoet.PropertySpec;
import com.squareup.kotlinpoet.TypeSpec;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import kotlin.Pair;

class GenerateRegexParser {

    public static void main(String[] args) throws IOException {
        String config = ConfigProvider.provide();

        Pair<List<String>, List<String>> parse = Parser.parse(config);
        Map<String, String> values = Parser.parseValues(parse.getSecond());

        if (values.containsKey("regex")) {
            String value = values.get("regex");

            String[] regexArray = value.split(",");

            runGenerator(regexArray);
        } else {
            runGenerator(new String[0]);
        }
    }

    private static void runGenerator(String[] elements) throws IOException {
        TypeSpec.Builder builder = TypeSpec.objectBuilder("AlderRegex")
                .addProperty(prepareList("regex", elements))
                .addFunction(generateParseFunction());

        TypeSpec helloWorld = builder.build();

        FileSpec spec = FileSpec.builder("hr.sevenofnine.agency.alder", "AlderConfig")
                .clearImports()
                .addType(helloWorld)
                .build();

        String file = spec.toString();
        file = file.replace("\nimport java.lang.String\n", "")
                .replace("java.lang.String", "String");

        System.out.write(file.getBytes());
        System.out.flush();
    }

    private static PropertySpec prepareList(String name, String[] elements) {
        ClassName list = new ClassName("kotlin.collections", "List");
        ClassName string = new ClassName("kotlin", "String");
        ParameterizedTypeName typeName = ParameterizedTypeName.get(list, string);

        return PropertySpec.builder(name, typeName)
                .initializer(generateList(elements))
                .build();
    }

    private static CodeBlock generateList(String[] elements) {
        CodeBlock.Builder block = new CodeBlock.Builder()
                .addStatement("listOf(")
                .indent();

        for (String element : elements) {
            block.addStatement("%S,", element);
        }
        block.unindent()
                .addStatement(")");

        return block.build();
    }

    private static FunSpec generateParseFunction() {
        return FunSpec.builder("parseWithRegex")
                .addParameter("string", String.class)
                .returns(String.class)
                .addStatement("var parsed = string")
                .addStatement("regex.forEach {")
                .addStatement("\tval regex = Regex(\"\\\"$it\\\"\\\\s?:\\\\s?\\\"([\\\\d\\\\w\\\\S\\\\s])\\\\s?(.*?)\\\"\")")
                .addStatement("\tif (parsed.contains(regex)) {")
                .addStatement("\t\tparsed = regex.replace(parsed, \"$it\\\":\\\"*\\\"\")")
                .addStatement("\t}")
                .addStatement("}")
                .addStatement("return parsed")
                .build();
    }
}
