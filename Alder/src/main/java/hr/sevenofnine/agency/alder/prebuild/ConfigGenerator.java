package hr.sevenofnine.agency.alder.prebuild;

import com.squareup.kotlinpoet.ClassName;
import com.squareup.kotlinpoet.FileSpec;
import com.squareup.kotlinpoet.PropertySpec;
import com.squareup.kotlinpoet.TypeSpec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kotlin.Pair;
import kotlin.reflect.KClass;
import kotlin.reflect.KType;

class ConfigGenerator {

    public static void main(String[] args) throws Exception {
        String str = ConfigProvider.provide();

        Pair<List<String>, List<String>> pair = Parser.parse(str);
        Map<String, String> objects = Parser.parseObjects(pair.getFirst());

        if (objects.containsKey("config")) {
            String object = objects.get("config");

            Pair<List<String>, List<String>> parse = Parser.parse(object);

            Map<String, String> stringStringMap = Parser.parseValues(parse.getSecond());
            runGenerator(new ArrayList(stringStringMap.keySet()), new ArrayList<>(stringStringMap.values()));
        }
    }

    private static void runGenerator(List<String> keys, List<String> values) throws IOException {
        TypeSpec.Builder builder = TypeSpec.objectBuilder("AlderConfig");

        for (int index = 0; index < keys.size(); index++) {
            String key = keys.get(index);
            String value = values.get(index);

            if (Parser.isNumeric(value)) {
                if (Parser.isDecimal(value)) {
                    builder.addProperty(PropertySpec.builder(key.toUpperCase(), new ClassName("kotlin", "Int"))
                            .initializer(value)
                            .build());
                } else {
                    builder.addProperty(PropertySpec.builder(key.toUpperCase(), Double.class)
                            .initializer(value)
                            .build());
                }
            } else {
                builder.addProperty(PropertySpec.builder(key.toUpperCase(), String.class)
                        .initializer("%S", value)
                        .build());
            }

        }
        TypeSpec helloWorld = builder.build();

        FileSpec spec = FileSpec.builder("hr.sevenofnine.agency.alder", "AlderConfig")
                .clearImports()
                .addType(helloWorld)
                .build();

        String file = spec.toString();
        file = file.replace("\nimport java.lang.String\n", "").replace("\nimport kotlin.Int\n", "");

        System.out.write(file.getBytes());
        System.out.flush();
    }

    static String getPath() {
        return System.getProperty("user.dir").replace("Alder", "app");// + "/app";
    }

}