package hr.sevenofnine.agency.alder.log

import android.content.Context
import android.content.Context.MODE_PRIVATE
import hr.sevenofnine.agency.alder.AlderFiles
import hr.sevenofnine.agency.alder.log.AlderUrlUtil.generateTag

object AlderPreferences {

    private const val LINKS_PREFERENCES = "hr.dturic.personal.tdkappdemoadler.shared_preferences"

    private const val DEVICE_ID = "hr.sevenofnine.agency.alder.DEVICE_ID"
    private const val USER_ID = "hr.sevenofnine.agency.alder.USER_ID"

    fun saveLink(context: Context, url: String, alderCategory: AlderFiles? = null) {
        val sharedPreferences = context.getPreferences()

        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = getUserId(context) ?: "*"
        val category = alderCategory?.getLogFileName() ?: "*"
        val tag = generateTag(deviceId, category, userId)

        sharedPreferences.edit().putString(tag, url).apply()
    }

    fun getLink(context: Context, category: AlderFiles?): String? {
        val preferences = context.getPreferences()
        val tag = generateTag(
            getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!"),
            category?.getLogFileName() ?: "*",
            getUserId(context) ?: "*"
        )

        return preferences.getString(tag, null)
    }

    fun removeLink(context: Context, category: String, userId: String? = null) {
        val deviceId = getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = userId ?: "*"

        context.getPreferences()
            .edit()
            .remove(generateTag(deviceId, category, userId))
            .apply()
    }

    fun saveDeviceId(context: Context, deviceId: String) {
        val sharedPreferences = context.getPreferences()

        sharedPreferences.edit()
            .putString(DEVICE_ID, deviceId)
            .apply()
    }

    fun removeUser(context: Context) = context.getPreferences()
        .edit()
        .remove(USER_ID)
        .apply()

    fun getDeviceId(context: Context) =
        context.getPreferences().getString(DEVICE_ID, null)

    fun saveUserId(context: Context, userId: String) {
        val sharedPreferences = context.getPreferences()

        sharedPreferences.edit()
            .putString(USER_ID, userId)
            .apply()
    }

    fun getUserId(context: Context) =
        context.getPreferences().getString(USER_ID, null)

    private fun Context.getPreferences() =
        getSharedPreferences(LINKS_PREFERENCES, MODE_PRIVATE)
}