package hr.sevenofnine.agency.alder.prebuild;

import com.squareup.kotlinpoet.FileSpec;
import com.squareup.kotlinpoet.FunSpec;
import com.squareup.kotlinpoet.KModifier;
import com.squareup.kotlinpoet.PropertySpec;
import com.squareup.kotlinpoet.TypeSpec;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import kotlin.Pair;

class GenerateCode {

    public static void main(String[] args) throws IOException {
        String str = ConfigProvider.provide();

        Pair<List<String>, List<String>> pair = Parser.parse(str);
        Map<String, String> values = Parser.parseValues(pair.getSecond());

        if (values.containsKey("files")) {
            String value = values.get("files");

            String[] filesArray = value.split(",");

            runGenerator(filesArray);
        } else {
            runGenerator(new String[0]);
        }
    }

    static void runGenerator(String[] files) throws IOException {
        TypeSpec.Builder builder = TypeSpec.enumBuilder("AlderFiles");
        builder.primaryConstructor(FunSpec.constructorBuilder()
                .addParameter("fileName", String.class, KModifier.PRIVATE)
                .build());

        builder.addProperty(PropertySpec.builder("fileName", String.class)
                .initializer("fileName")
                .build());

        for (String file : files) {
            builder.addEnumConstant(file.toUpperCase(), TypeSpec.anonymousClassBuilder()
                    .addSuperclassConstructorParameter("%S", file)
                    .build());
        }
        builder.addFunction(generateGetterFunction());

        FileSpec fileSpec = FileSpec.builder("hr.sevenofnine.agency.alder", "AlderFiles")
                .addType(builder.build())
                .build();

        String file = fileSpec.toString();
        file = file.replace("\nimport java.lang.String\n", "");

        System.out.write(file.getBytes());
        System.out.flush();
    }

    private static FunSpec generateGetterFunction() {
        return FunSpec.builder("getLogFileName")
                .returns(String.class)
                .addStatement("return this.fileName")
                .build();
    }
}