package hr.sevenofnine.agency.alder.prebuild;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

class ConfigProvider {
    public static String provide() throws IOException {
        File file = new File(getPath(), "alder_config.config");

        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        return new String(data, "UTF-8");
    }

    static String getPath() {
        return System.getProperty("user.dir").replace("Alder", "app");//+ "/app";
    }
}
