package hr.sevenofnine.agency.alder.prebuild;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import kotlin.Pair;
import kotlin.text.MatchResult;
import kotlin.text.Regex;

public class Parser {
    private static String OBJECT_REGEX = "[\\w]*=\\[[\\d\\w\\s\\S]*(.*?)(?=\\])\\]";
    private static String VALUE_REGEX = "[\\w]*=[\\d\\w\\s\\S](.*?)(?=\\n|$)";

    private static String KEY_VALUE_REGEX = "(\\w+)=([^\\s]+)";
    private static String KEY_OBJECT_REGEX = "([\\w]*)=\\[([\\d\\w\\s\\S]*(.*?)(?=\\]))\\]";

    public static Pair<List<String>, List<String>> parse(String config) {
        Pair<List<String>, String> result = getObjects(config);
        List<String> values = getValues(result.getSecond());

        return new Pair<>(result.getFirst(), values);
    }

    private static Pair<List<String>, String> getObjects(String config) {
        String newConfig = config;
        List<String> list = new LinkedList<>();
        Regex regex = new Regex(OBJECT_REGEX);

        Iterator<MatchResult> results = regex.findAll(config, 0).iterator();

        while (results.hasNext()) {
            MatchResult next = results.next();
            String object = next.getValue();

            list.add(object);
            newConfig = newConfig.replace(object, "");
        }

        return new Pair<>(list, newConfig);
    }

    private static List<String> getValues(String config) {
        List<String> list = new LinkedList<>();
        Regex regex = new Regex(VALUE_REGEX);

        Iterator<MatchResult> results = regex.findAll(config, 0).iterator();

        while (results.hasNext()) {
            MatchResult next = results.next();
            String object = next.getValue();

            list.add(object);
        }

        return list;
    }

    static Map<String, String> parseValues(List<String> values) {
        HashMap<String, String> map = new LinkedHashMap<>();
        Regex regex = new Regex(KEY_VALUE_REGEX);

        for (String phrase : values) {
            MatchResult result = regex.find(phrase, 0);

            if (result != null) {
                List<String> results = result.getGroupValues();
                String key = results.get(1);
                String value = results.get(2);

                map.put(key, value);
            }
        }

        return map;
    }

    static Map<String, String> parseObjects(List<String> values) {
        HashMap<String, String> map = new LinkedHashMap<>();
        Regex regex = new Regex(KEY_OBJECT_REGEX);

        List<Pair<String, String>> parsedValues = new LinkedList<>();
        for (String phrase : values) {
            MatchResult result = regex.find(phrase, 0);

            if (result != null) {
                List<String> results = result.getGroupValues();
                String key = results.get(1);
                String value = results.get(2);

                map.put(key, value);
            }
        }

        return map;
    }

    static boolean isNumeric(String value) {
        return value.matches("-?\\d+(\\d+)?");
    }

    static boolean isDecimal(String value) {
        return value.matches("-?\\d+(\\.\\d+)?");
    }

    static boolean isBoolean(String value) {
        return value.matches("(?i)true|false");
    }

    static boolean isArray(String value) {
        return value.contains(",");
    }
}
