package hr.sevenofnine.agency.alder.log

import android.app.ActivityManager
import android.content.Context
import hr.sevenofnine.agency.alder.AlderConfig
import hr.sevenofnine.agency.alder.AlderFiles
import hr.sevenofnine.agency.alder.log.AlderUrlUtil.generateTag
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileReader
import java.io.FileWriter

object AlderCacheManager {

    private lateinit var context: Context

    fun init(context: Context) {
        this.context = context

        println(cacheFileSize)
        println(cacheFileTime)

        CacheTimerThread(context, cacheFileTime).start()
    }

    private val cacheFileSize: Number
        get() {
            return try {
                val test =
                    AlderConfig.javaClass.kotlin.members.filter { it.name.contains("CACHE_SIZE") }
                test[0].call(AlderConfig) as Number
            } catch (e: Exception) {
                60000
            }
        }

    private val cacheFileTime: Number
        get() {
            return try {
                val test =
                    AlderConfig.javaClass.kotlin.members.filter { it.name.contains("CACHE_TIME") }
                test[0].call(AlderConfig) as Number
            } catch (e: Exception) {
                102400
            }
        }

    fun cache(log: String, category: AlderFiles?) {
        val deviceId =
            AlderPreferences.getDeviceId(context) ?: throw RuntimeException("DeviceId not set!!")
        val userId = AlderPreferences.getUserId(context) ?: "*"

        val file = getCacheFile(deviceId, userId, category)

        FileWriter(file, true).use {
            it.appendLine(log)
            it.flush()
        }

        if (file.length() >= cacheFileSize.toLong()) sendLog(context)
    }

    private fun sendLog(context: Context) {
        val deviceId = AlderPreferences.getDeviceId(context) ?: ""
        val userId = AlderPreferences.getUserId(context)

        for (value in AlderFiles.values()) {
            val file = getCacheFile(deviceId, userId, value)

            val body = FileReader(file).readText()

            if (body.isBlank()) continue
            Alder.sendLog(body, value)
            FileWriter(file).write("")
        }

        val file = getCacheFile(deviceId, userId, null)
        val body = FileReader(file).readText()

        if (body.isBlank()) return
        Alder.log(body)
        FileWriter(file).write("")
    }

    private fun getCacheFile(deviceId: String, userId: String?, category: AlderFiles?): File {
        val userId = userId ?: "*"
        val category = category?.name ?: "*"

        val fileName = generateTag(deviceId, category, userId)

        val cacheDir = context.cacheDir

        return with(File(cacheDir, "alderCache")) {
            if (!exists()) mkdir()

            val cacheFile = File(this, "$fileName.log")

            if (!cacheFile.exists()) cacheFile.createNewFile()

            cacheFile
        }
    }

    class CacheTimerThread(private val context: Context, private val delay: Number) : Thread() {

        override fun run() {
            super.run()

            MainScope().launch(Dispatchers.Default) {
                try {
                    logic()
                } catch (e: Exception) {
                    interrupt()
                }
            }
        }

        private suspend fun logic() {
            while (isAppRunning(context)) {
                delay(delay.toLong())
//                sendLog(context)
                println("Cached log data sent!")
            }
        }

        private fun isAppRunning(context: Context): Boolean {
            val activityManager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val processInfos = activityManager.runningAppProcesses
            for (processInfo in processInfos) {
                if (processInfo.processName == context.packageName) {
                    return true
                }
            }
            return false
        }
    }
}